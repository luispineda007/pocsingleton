//
//  Page3ViewModel.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import Foundation

class Page3ViewModel: ObservableObject {
    
    private static var sharedInstance: Page3ViewModel?
    private static var config: Config?
    
    @Published var num: Int
    var msg: ((String) -> Void)? = nil
    
    struct Config {
        var num: Int
        var msg: ((String) -> Void)? = nil
    }
    
    class func setup(config: Config){
        Page3ViewModel.config = config
    }
    
    class var shared: Page3ViewModel {
        guard let shared = self.sharedInstance else {
            let sharedInstance = Page3ViewModel()
            self.sharedInstance = sharedInstance
            return sharedInstance
        }
        return shared
    }
    
    private init() {
        guard let config = Page3ViewModel.config else {
            fatalError("Error - you must call setup before accessing Page3ViewModel.shared")
        }
        
        self.num = config.num
        self.msg = config.msg
    }
    
    class func destroy() {
        sharedInstance = nil
        config = nil
    }
    
}
