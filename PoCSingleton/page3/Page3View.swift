//
//  Page3View.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import SwiftUI

struct Page3View: View {
    
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var page3VM: Page3ViewModel = Page3ViewModel.shared
    
    var body: some View {
        VStack {
            Button("Salir sin destruir") {
                presentationMode.wrappedValue.dismiss()
            }
            Spacer()
            Text("El valor de num: \(page3VM.num)")
            
            Button(action: {
                page3VM.num += 1
            }, label: {
                Text("sumar")
            })
            
            
            Spacer()
            Button("Destruir la Instancia") {
                Page3ViewModel.destroy()
                presentationMode.wrappedValue.dismiss()
            }
        }.padding()
    }
}

struct Page3View_Previews: PreviewProvider {
    static var previews: some View {
        Page3View()
    }
}
