//
//  Page2View.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import SwiftUI

struct Page2View: View {
    
    @StateObject var page2VM = Page2ViewModel()
    @State private var isPresented = false
    
    var body: some View {
        VStack(spacing: 50.0) {
            Text(page2VM.title)
                .foregroundColor(page2VM.color)
            Button(action: {
                page2VM.title = "Pagina 2"
            }, label: {
                Text("Cambiar Titulo")
            })
            Button(action: {
                page2VM.color = Color.blue
            }, label: {
                Text("Cambiar Color")
            })
            
            
            //Estos dos se puede hacer algo para que cuando la instancia este iniciada no aparezca lanzar sino
            // Solo retomar la vista, para retomar NO lanzamos la configuracion o los valores inicales 
            
            
            Button(action: {
                Page3ViewModel.setup(config: Page3ViewModel.Config(num: 100,
                                                                   msg: { msg in
                    print("MSG: \(msg)")
                }))
                isPresented.toggle()
            }, label: {
                Text("Lanzar Vista 3")
            })
            
            Button(action: {
                isPresented.toggle()
            }, label: {
                Text("Retomar Vista 3 solo si ya se lanzo")
            })
        }.fullScreenCover(isPresented: $isPresented, content: {
            Page3View()
        })
    }
}

struct Page2View_Previews: PreviewProvider {
    static var previews: some View {
        Page2View()
    }
}
