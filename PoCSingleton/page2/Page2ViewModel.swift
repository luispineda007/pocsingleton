//
//  Page2ViewModel.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import SwiftUI

class Page2ViewModel: BaseViewModel {

    @Published var color: Color = Color.red
    
}
