//
//  PoCSingletonApp.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import SwiftUI

@main
struct PoCSingletonApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
