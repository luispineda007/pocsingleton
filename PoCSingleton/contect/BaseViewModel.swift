//
//  BaseViewModel.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import Foundation

class BaseViewModel: ObservableObject {
    
    @Published var title: String = "Pagina Inicial"
    @Published var name: String = ""
    
}
