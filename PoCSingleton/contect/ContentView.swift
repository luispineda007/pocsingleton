//
//  ContentView.swift
//  PoCSingleton
//
//  Created by Luis Pineda on 27/05/21.
//

import SwiftUI

struct ContentView: View {
    
    @StateObject var baseVM: BaseViewModel = BaseViewModel()
    
    
    var body: some View {
        NavigationView {
            VStack(spacing: 50.0) {
                Text(baseVM.title)
                    .padding()
                Button(action: {
                    baseVM.title = "Pagina 1 del INICIO"
                }, label: {
                    Text("Cambiar Titulo")
                })
                NavigationLink(
                    destination: Page2View(),
                    label: {
                        Text("Navigate Page 2")
                    })
                
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
